%% Ryan Zimcosky
%% CS3060
%% Reflection 7
%% function1.erl

-  module(function1).
-  export([hello_world/0]).
-  export([num_char/1]).
-  export([num_word/1]).
-  export([count/1]).
-  export([validate/1]).
-  export([return_value/2]).

%% -------------------------
%%          Task 1
%% -------------------------

hello_world() ->
    io:fwrite("Hello world\n").

%% -------------------------
%%          Task 2
%% -------------------------

num_char([]) -> 0;
num_char([_|T]) -> 
    1 + num_char(T).

%% -------------------------
%%          Task 3
%% -------------------------
num_word(String) ->
    num_word(String, 0).

num_word([], Count) -> 
    Count;

num_word([Last], Count) when (Last =/= ($ )) -> 
    Count + 1;
num_word([First, H|T], Count) when (First =/= ($ )) and (H =:= ($ )) ->
    num_word([H|T], Count + 1);

num_word([_|T], Count) -> 
    num_word(T, Count).

%% -------------------------
%%          Task 4
%% -------------------------
count(0) -> 0;
count(N) -> count(N,0).

count(1, Count) -> 
    io:format("~B, ", [Count+1]);
count(N, Count) -> 
    io:format("~B, ", [Count+1]),
    count(N - 1, Count + 1).

%% -------------------------
%%          Task 5
%% -------------------------

validate(success) -> io:fwrite("success\n");
validate({error, Message}) -> io:format("error: ~s ~n", [Message]).

%% -------------------------
%%          Task 6
%% -------------------------

 return_value(List, Keyword) ->

     Match = [Value || {Key, Value} <- List, (Keyword == Key)],

     case Match of
        [H|_]  -> H;
        [] -> false
     end.

