# CS3060_FA2018_R07_ZIMCOSKY

## Sample Run

```erlang
1> c('function1.erl').
{ok,function1}
2> function1:hello_world().
Hello world
ok
3> function1:num_char(function1:hello_world()).
Hello world
** exception error: no function clause matching
                    function1:num_char(ok) (function1.erl, line 25)
4> function1:num_char("Hello world").
11
5> function1:num_word("Hello world").
2
6> function1:count(10).
1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ok
8> function1:validate(success).
success
ok
9> List = [{erlang, "a functional language"}, {ruby, "an OO language"}].
[{erlang,"a functional language"},{ruby,"an OO language"}]
10>
10> function1:return_value(List, ruby).
"an OO language"
11>
```

## Reflection Questions

1. What was the major question or topic addressed in the classroom?
   - What is Erlang, and how do we accomplish tasks using this language?

2. Summarize what you learned during this week of class. Note any particular features, concepts, code,or terms that you believe are important.
   - I learned how to write in Erlang. I learned syntax and its programing style, which is similar to prolog. I learned different ways to produce output, how to count elements of a list, how to count occurences of elements in a list, how to comprehend lists, and use filtering.

3. What did you learn about the current language that is different from what you have previously experienced? Do you think it is a good difference or bad difference?

4. Do you learn answers to any of the 5 fundamental questions we are asking about each programming language this week?

   1. What is the typing model?
      - Strongly typed

   2. What is the programming model?
      - Functional language

   3. Is the language Interpreted or Compiled?
      - Both

   4. What are the decision constructs & core data structures?

   5. What makes this language unique?

5. What differences did you see in the implementations of various tasks among the class?
   - I did was not present in class.